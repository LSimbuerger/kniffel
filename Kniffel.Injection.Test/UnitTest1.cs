using System;
using System.Net;
using Xunit;
using Kniffel.Lib;
using Kniffel.Server;
using Kniffel.Server.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Kniffel.Injection.Test
{
    public class UnitTest1
    {
        [Fact]
        public void GetDiceThrown_Invalid_Input()
        {
            // Arrange.
            var mockDice = new Mock<IDice>();
            int[] possibleValues = { 1, 2, 3, 4, 5 };
            mockDice.Setup(x => x.Throw(It.IsNotIn(possibleValues))).Throws(new ArgumentOutOfRangeException());
            var controller = new KniffelController(mockDice.Object);

            // Act.
            var have = controller.GetDiceThrow(7).Result;

            // Assert.
            Assert.IsType<BadRequestObjectResult>(have);
        }

        [Theory]
        [InlineData(new int[] { 1, 1, 1, 2, 2 })]
        [InlineData(new int[] { 4, 4, 4, 2 })]
        [InlineData(new int[] { 1, 1, 1 })]
        [InlineData(new int[] { 3, 3})]
        [InlineData(new int[] { 1})]
        public void GetDiceThrown_Return_Right_Size(int[] input)
        {
            // Arrange.
            var mockDice = new Mock<IDice>();
            var possibleValues = new int[] {1, 2, 3, 4, 5};

            mockDice.Setup(x => x.Throw(It.IsIn(possibleValues))).Returns(input);
            var controller = new KniffelController(mockDice.Object);

            // Act.
            var have = (OkObjectResult)controller.GetDiceThrow(input.Length).Result;

            // Assert.
            Assert.Equal(input, have.Value);

        }

        [Fact]
        public void PostCalculation_Invalid_Request()
        {
            // Arrange.
            var mockDice = new Mock<IDice>();
            int[] haveArray = { 1, 2, 3, 4, 5 };

            mockDice.Setup(x => x.Calculate(
                It.Is<int[]>(j => j.Length == 5),
                It.IsAny<Categories>()))
                .Throws(new ArgumentOutOfRangeException());

            var controller = new KniffelController(mockDice.Object);

            // Act.
            var have = controller.PostCalc(haveArray, (int)Categories.ACES).Result;

            // Assert.
            Assert.IsType<BadRequestObjectResult>(have);
        }

        [Fact]
        public void PostCalculation()
        {
            // Arrange.
            Random random = new Random();
            var testDictionary = new Dictionary<Categories, int?>();
            foreach (Categories categories in Enum.GetValues(typeof(Categories)))
            {
                testDictionary.Add(categories, null);
            }

            // TODO: Not finished.
            var mockDice = new Mock<IDice>();
            mockDice.Setup(x => x.Calculate(
                It.Is<int[]>(j => j.Length == 5),
                It.IsAny<Categories>()));
        }

    }
}
