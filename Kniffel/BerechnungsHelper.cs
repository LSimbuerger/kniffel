﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kniffel
{
    public static class BerechnungsHelper
    {
        /// <summary>
        /// Calculates the score for a street.
        /// </summary>
        /// <param name="array">All active dice values.</param>
        /// <returns>The score.</returns>
        public static int CalcStreet(int[] array)
        {
            Array.Sort(array);
            // Check for full street.
            bool fullStreet = true;
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] - array[i - 1] != 1) fullStreet = false;
            }
            if (fullStreet) return 40;
            // Check for small street.
            bool smallStreetCount = true;
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] - array[i - 1] != 1 && array[i] != array[i-1]) smallStreetCount = false;
            }
            if (smallStreetCount) return 30;
            return 0;

        }

        // Veronika
        public static int BerechnePasch(int[] wuerfe)
        {
            int result = 0;
            bool pasch = false;

            int[] amount = new int[6];

            for (int i = 0; i < wuerfe.Length; i++) amount[wuerfe[i] - 1]++;

            for (int i = 0; i < amount.Length; i++)
            {
                if (amount[i] == 3 || amount[i] == 4) pasch = true;
            }

            if (pasch)
            {
                foreach (int w in wuerfe)
                {
                    result = result + w;
                }
            }
            return result;
        }

        // Dmytro
        public static int FullHouse(int[] array)
        {
            int[] counts = new int[6 + 1];
            bool found2 = false;
            bool found3 = false;

            foreach (int die in array)
            {
                counts[die]++;
            }
            foreach (int count in counts)
            {
                if (count == 2) found2 = true;
                else if (count == 3) found3 = true;
            }
            return (found2 && found3) ? 25 : 0;
        }

        /// <summary>
        /// This function is for counting the corresponding numbers.
        /// </summary>
        /// <param name="array">The input array.</param>
        /// <param name="zeile">The number, which should be countet.</param>
        /// <returns></returns>
        public static int Zaehler(int[] array, Zeile zeile)
        {
            int suche = (int)zeile + 1;
            int sum = 0;
            foreach(var augen in array)
            {
                if (augen == suche)
                    sum += augen;
            }
            return sum;
        }

        /// <summary>
        /// Checks if the values of the input array are all the same.
        /// </summary>
        /// <param name="array">The input array</param>
        /// <returns></returns>
        public static int AlleGleich(int[] array)
        {
            for(int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] != array[i + 1])
                    return 0;
            }
            return 50;
        }

        // Lukas
        public static int Chance(int[] array)
        {
            int sum = 0;
            foreach(var augen in array)
            {
                sum += augen;
            }
            return sum;
        }
    }
}
