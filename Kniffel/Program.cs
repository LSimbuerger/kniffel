﻿using Kniffel;
using System;

namespace kniffel_test
{
    class Program
    {
        static void Main(string[] args)
        {
            var testPlayer = new Player();
            Console.WriteLine("Endergebniss: " + String.Join(", ", testPlayer.Play()));
        }
    }
}
