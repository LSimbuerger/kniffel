﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kniffel
{
    public class Player
    {
        private int[] wuerfelArray;

        public Player()
        {
            wuerfelArray = new int[5];
        }

        public int[] Play()
        {
            int aktiveWuerfel = 5;
            int[] endArray = new int[5];
            int[] result = new int[5];
            for (int i = 0; i < 3; i++)
            {
                // Wenn keine Würfel mehr vorhanden => ende der funktion.
                if (aktiveWuerfel == 0)
                {
                    break;
                }

                result = Wuerfeln(aktiveWuerfel);
                Console.WriteLine("Wurfergebnis: " + String.Join(", ", result));
                Console.WriteLine("Welche Würfel sollen behalten werden?");
                string behalten = Console.ReadLine();
                string[] behaltenString;
                // TODO: check if input is valid.
                if (String.IsNullOrEmpty(behalten))
                {
                    continue;
                } else if(!behalten.Contains(" "))
                {
                    int newNumber = int.Parse(behalten);
                    endArray[5 - aktiveWuerfel] = newNumber;
                    result = result.Where(val => val != newNumber).ToArray();
                    aktiveWuerfel -= 1;
                    continue;
                } 
                behaltenString = behalten.Split(" ");
                int[] behaltenInt = Array.ConvertAll(behaltenString, b => int.Parse(b));

                
                for (int j = 5 - aktiveWuerfel; j < behaltenInt.Length + (5 - aktiveWuerfel) ; j++)
                {
                     endArray[j] = behaltenInt[j - (5 - aktiveWuerfel)];
                }
                aktiveWuerfel -= behaltenInt.Length;
            }
            // Wenn noch aktive Würfel => auffüllen.
            if (aktiveWuerfel != 0) 
            {
                for (int j = 5 - aktiveWuerfel; j < endArray.Length ; j++)
                {
                    endArray[j] = result[j - (5 - aktiveWuerfel)];
                }
            }
            return endArray;
        }

        /// <summary>
        /// This function create's an array (length depends on the throwCount parameter) with numbers between 1-6. 
        /// </summary>
        /// <param name="throwCount">This represtents how many dices should be thrown.</param>
        /// <returns>An array consisting out of the generated dices throws.</returns>
        public int[] Wuerfeln(int throwCount)
        {
            if (throwCount < 0 || throwCount > 5)
            {
                throw new ArgumentOutOfRangeException();
            }
            int[] dice = new int[throwCount];
            Random random = new Random();
            for (int i = 0; i < throwCount; i++)
            {
                dice[i] = random.Next(6) + 1;
            }
            return dice;
        }

        /// <summary>
        /// Calc the score for the selected <see cref="Zeile"/>
        /// </summary>
        /// <param name="array">Then input array.</param>
        /// <param name="zeile">The selected <see cref="Zeile"/>-Type</param>
        /// <returns></returns>
        private int Berechne(int[] array, Zeile zeile)
        {
            switch (zeile)
            {
                case Zeile.Einer:
                case Zeile.Zweier:
                case Zeile.Dreier:
                case Zeile.Vierer:
                case Zeile.Fuenfer:
                case Zeile.Sechner:
                    return BerechnungsHelper.Zaehler(array, zeile);
                case Zeile.Dreierpasch:
                    // TODO: diff between dreier and vierer.
                case Zeile.Viererpasch:
                    return BerechnungsHelper.BerechnePasch(array);
                case Zeile.GrosseStrasse:
                case Zeile.KleineStrasse:
                    return BerechnungsHelper.CalcStreet(array);
                case Zeile.Chance:
                    return BerechnungsHelper.Chance(array);
                case Zeile.Kniffel:
                    return BerechnungsHelper.AlleGleich(array);
                case Zeile.FullHouse:
                    return BerechnungsHelper.FullHouse(array);
                default:
                    throw new ArgumentException();
            }
        }



    }
}