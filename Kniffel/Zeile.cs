﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kniffel
{
    public enum Zeile
    {
        Einer,
        Zweier,
        Dreier,
        Vierer,
        Fuenfer,
        Sechner,
        Dreierpasch,
        Viererpasch,
        FullHouse,
        KleineStrasse,
        GrosseStrasse,
        Kniffel,
        Chance
    }
}
