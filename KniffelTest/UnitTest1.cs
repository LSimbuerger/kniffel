using Xunit;
using Kniffel.Lib;
using Kniffel.Server.Services;

namespace KniffelTest
{
    public class UnitTest1
    {
        // Test 3 Of A Kind
        [Theory]
        [InlineData(new int[] { 1, 1, 1, 2, 2 }, 7)]
        [InlineData(new int[] { 1, 2, 1, 2, 2 }, 8)]
        [InlineData(new int[] { 6, 6, 6, 6, 5 }, 29)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 5)]
        [InlineData(new int[] { 1, 1, 4, 2, 2 }, 0)]
        public void ThreeOfAKindTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcThreeOfAKind(input), score);
        }

        // Test 4 Of A Kind
        [Theory]
        [InlineData(new int[] { 6, 6, 6, 6, 5 }, 29)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 5)]
        [InlineData(new int[] { 1, 1, 1, 2, 2 }, 0)]
        [InlineData(new int[] { 1, 1, 4, 2, 2 }, 0)]
        public void FourOfAKindTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcFourOfAKind(input), score);
        }

        // Test Full House
        [Theory] 
        [InlineData(new int[] { 1, 1, 1, 2, 2 }, 25)]
        [InlineData(new int[] { 3, 3, 3, 2, 2 }, 25)]
        [InlineData(new int[] { 1, 2, 1, 2, 2 }, 25)]
        [InlineData(new int[] { 4, 2, 4, 4, 2 }, 25)]
        [InlineData(new int[] { 4, 4, 4, 1, 1 }, 25)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 0)]
        [InlineData(new int[] { 1, 1, 4, 2, 2 }, 0)]
        public void FullHouseTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcFullHouse(input), score);
        }

        // Test Small Straight
        [Theory]
        // Large Straight
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 30)]
        [InlineData(new int[] { 2, 3, 4, 5, 6 }, 30)]
        [InlineData(new int[] { 3, 4, 5, 6, 2 }, 30)]
        // Small Straight 
        [InlineData(new int[] { 1, 2, 4, 3, 1 }, 30)]
        [InlineData(new int[] { 6, 3, 4, 5, 3 }, 30)]
        [InlineData(new int[] { 3, 2, 5, 4, 5 }, 30)]
        // Random
        [InlineData(new int[] { 1, 2, 4, 5, 6 }, 0)]
        [InlineData(new int[] { 3, 3, 1, 5, 2 }, 0)]
        [InlineData(new int[] { 2, 3, 1, 5, 2 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 5, 2 }, 0)]
        [InlineData(new int[] { 1, 2, 5, 5, 6 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 0)]
        public void SmallStraightTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcSmallStraight(input), score);
        }

        // Test Large Straight
        [Theory]
        // Large Straight
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 40)]
        [InlineData(new int[] { 3, 4, 5, 6, 2 }, 40)]
        [InlineData(new int[] { 2, 3, 4, 5, 6 }, 40)]
        // Small Straight
        [InlineData(new int[] { 1, 2, 4, 3, 1 }, 0)]
        [InlineData(new int[] { 6, 3, 4, 5, 3 }, 0)]
        [InlineData(new int[] { 3, 2, 5, 4, 5 }, 0)]
        // Random
        [InlineData(new int[] { 1, 2, 4, 5, 6 }, 0)]
        [InlineData(new int[] { 3, 3, 1, 5, 2 }, 0)]
        [InlineData(new int[] { 2, 3, 1, 5, 2 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 5, 2 }, 0)]
        [InlineData(new int[] { 1, 2, 5, 5, 6 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 0)]
        public void LargeStraightTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcLargeStraight(input), score);
        }

        // Test Totals
        [Theory]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, Categories.ACES, 5)]
        [InlineData(new int[] { 2, 2, 2, 1, 1 }, Categories.TWOS, 6)]
        [InlineData(new int[] { 1, 2, 4, 5, 6 }, Categories.THREES, 0)]
        [InlineData(new int[] { 1, 4, 4, 4, 1 }, Categories.FOURS, 12)]
        [InlineData(new int[] { 5, 2, 1, 4, 5 }, Categories.FIVES, 10)]
        [InlineData(new int[] { 1, 1, 6, 1, 1 }, Categories.SIXES, 6)]
        public void CounterTest(int[] input, Categories category, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcCounter(input, category), score);
        }

        // Test Yahtzee
        [Theory]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 50)]
        [InlineData(new int[] { 2, 2, 2, 2, 2 }, 50)]
        [InlineData(new int[] { 3, 3, 3, 3, 3 }, 50)]
        [InlineData(new int[] { 4, 4, 4, 4, 4 }, 50)]
        [InlineData(new int[] { 5, 5, 5, 5, 5 }, 50)]
        [InlineData(new int[] { 6, 6, 6, 6, 6 }, 50)]
        [InlineData(new int[] { 6, 6, 6, 6, 5 }, 0)]
        [InlineData(new int[] { 2, 4, 1, 6, 5 }, 0)]
        public void YahtzeeTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcYahtzee(input), score);
        }

        // Test Chance
        [Theory]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 5)]
        [InlineData(new int[] { 2, 2, 2, 1, 1 }, 8)]
        [InlineData(new int[] { 1, 2, 4, 5, 6 }, 18)]
        [InlineData(new int[] { 1, 4, 4, 4, 1 }, 14)]
        [InlineData(new int[] { 5, 2, 1, 4, 5 }, 17)]
        [InlineData(new int[] { 1, 1, 6, 1, 1 }, 10)]
        public void ChanceTest(int[] input, int score)
        {
            DiceServer diceServer = new DiceServer();
            Assert.Equal(diceServer.CalcChance(input), score);
        }

        // Test Throw
        [Theory]
        [InlineData(5)]
        [InlineData(2)]
        [InlineData(0)]
        public void ThrowTest(int diceCount)
        {
            DiceServer diceServer = new DiceServer();
            int[] result = diceServer.Throw(diceCount);
            Assert.Equal(result.Length, diceCount);
        }
    }

}
