import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KniffelContainerComponent } from './kniffel-container/kniffel-container.component';
import { KniffelApiService } from './services/kniffel-api.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    KniffelContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [KniffelApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
