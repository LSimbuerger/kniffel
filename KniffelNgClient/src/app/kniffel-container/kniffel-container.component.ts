import { Component, OnInit } from '@angular/core';
import { KniffelApiService } from '../services/kniffel-api.service';
import { Categories } from '../models/categories.enum';
import { KniffelResultSheet } from '../models/kniffel-result-sheet.interface';

@Component({
  selector: 'app-kniffel-container',
  templateUrl: './kniffel-container.component.html',
  styleUrls: ['./kniffel-container.component.css']
})
export class KniffelContainerComponent implements OnInit {

  public diceResults: number[] = [0, 0, 0, 0, 0];
  public dicesToReRoll: number[] = [];

  public rollCounter: number = 0;
  public turnEnded: boolean = false;

  public categoryFields: { title: string, value: Categories }[] = [
    { title: 'ACES', value: Categories.ACES },
    { title: 'TWOS', value: Categories.TWOS },
    { title: 'THREES', value: Categories.THREES },
    { title: 'FOURS', value: Categories.FOURS },
    { title: 'FIVES', value: Categories.FIVES },
    { title: 'SIXES', value: Categories.SIXES },
    { title: 'THREE_OF_A_KIND', value: Categories.THREE_OF_A_KIND },
    { title: 'FOUR_OF_A_KIND', value: Categories.FOUR_OF_A_KIND },
    { title: 'FULL_HOUSE', value: Categories.FULL_HOUSE },
    { title: 'SMALL_STRAIGHT', value: Categories.SMALL_STRAIGHT },
    { title: 'LARGE_STRAIGHT', value: Categories.LARGE_STRAIGHT },
    { title: 'YAHTZEE', value: Categories.YAHTZEE },
    { title: 'CHANCE', value: Categories.CHANCE }
  ];

  public resultSheet: KniffelResultSheet = {
    ACES: undefined,
    CHANCE: undefined,
    FIVES: undefined,
    FOURS: undefined,
    FOUR_OF_A_KIND: undefined,
    FULL_HOUSE: undefined,
    LARGE_STRAIGHT: undefined,
    SIXES: undefined,
    SMALL_STRAIGHT: undefined,
    THREES: undefined,
    THREE_OF_A_KIND: undefined,
    TWOS: undefined,
    YAHTZEE: undefined
  }

  constructor(private kniffelApi: KniffelApiService) { }

  ngOnInit() {
    this.kniffelApi.throw(5).subscribe(result => {
      this.diceResults = result.map(x => x);
    });
  }


  public registerForReRoll(index: number) {
    this.dicesToReRoll.push(index);
  }

  public rollDices() {
    this.kniffelApi.throw(this.dicesToReRoll.length).subscribe(result => {
      this.dicesToReRoll.forEach((dice, i) => {
        this.diceResults[dice] = result[i];
      });

      this.dicesToReRoll = [];

      this.rollCounter++;

      if (this.rollCounter === 2) this.turnEnded = true;
    });
  }

  public selectCategory(category: Categories) {
    console.log('Sending to the api...', this.diceResults, category);

    this.kniffelApi.calculate(this.diceResults, category).subscribe(result => {
      this.resultSheet = result;
      this.turnEnded = false;
      this.rollCounter = 0;
    });
  }
}

