import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KniffelContainerComponent } from './kniffel-container.component';

describe('KniffelContainerComponent', () => {
  let component: KniffelContainerComponent;
  let fixture: ComponentFixture<KniffelContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KniffelContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KniffelContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
