import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KniffelContainerComponent } from './kniffel-container/kniffel-container.component';

const routes: Routes = [
  { path: '', component: KniffelContainerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
