export interface KniffelResultSheet {
    ACES: number
    CHANCE: number
    FIVES: number
    FOURS: number
    FOUR_OF_A_KIND: number
    FULL_HOUSE: number
    LARGE_STRAIGHT: number
    SIXES: number
    SMALL_STRAIGHT: number
    THREES: number
    THREE_OF_A_KIND: number
    TWOS: number;
    YAHTZEE: number;
}