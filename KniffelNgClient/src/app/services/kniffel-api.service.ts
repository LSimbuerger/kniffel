import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Categories } from '../models/categories.enum';
import { KniffelResultSheet } from '../models/kniffel-result-sheet.interface';

@Injectable({
  providedIn: 'root'
})
export class KniffelApiService {
  private _apiEndoint = environment.apiEndoint;

  constructor(private http: HttpClient) { }

  public throw(diceCount: number): Observable<number[]> {
    return this.http.get<number[]>(`${this._apiEndoint}/kniffel/throw/${diceCount}`);
  }

  public calculate(dices: number[], category: Categories): Observable<KniffelResultSheet> {
    return this.http.post<KniffelResultSheet>(`${this._apiEndoint}/kniffel/calculate/${category}`, dices);
  }

}
