import { TestBed } from '@angular/core/testing';

import { KniffelApiService } from './kniffel-api.service';

describe('KniffelApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KniffelApiService = TestBed.get(KniffelApiService);
    expect(service).toBeTruthy();
  });
});
