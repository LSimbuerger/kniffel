﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Kniffel.Lib;

namespace Kniffel.Server
{
    public class Program
    {

        // This is only temporary.
        public static IDictionary<Categories, int?> dataStorage;

        public static void Main(string[] args)
        {
            dataStorage = new Dictionary<Categories, int?>();
            foreach (Categories categories in Enum.GetValues(typeof(Categories)))
            {
                dataStorage.Add(categories, null);
            }

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
