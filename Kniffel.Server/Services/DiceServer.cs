﻿using System;
using System.Collections.Generic;
using Kniffel.Lib;

namespace Kniffel.Server.Services
{
    public class DiceServer : IDice
    {
        
        private IDictionary<Categories, int?> InsertScore(int value, Categories categories)
        {
            if (Program.dataStorage[categories] != null)
            {
                throw new ArgumentException();
            }
            Program.dataStorage[categories] = value;
            return Program.dataStorage;
        }

        /// <summary>
        /// This will calculate the score point of the given combination.
        /// </summary>
        /// <param name="dice">The current combination.</param>
        /// <param name="categories">The selected categorie.</param>
        /// <returns></returns>
        public IDictionary<Categories, int?> Calculate(int[] dice, Categories categories)
        {
            switch (categories)
            {
                case Categories.ACES:
                case Categories.TWOS:
                case Categories.THREES:
                case Categories.FOURS:
                case Categories.FIVES:
                case Categories.SIXES:
                    return InsertScore(CalcCounter(dice, categories), categories);
                case Categories.THREE_OF_A_KIND:
                    return InsertScore(CalcThreeOfAKind(dice), categories);
                case Categories.FOUR_OF_A_KIND:
                    return InsertScore(CalcFourOfAKind(dice), categories);
                case Categories.FULL_HOUSE:
                    return InsertScore(CalcFullHouse(dice), categories);
                case Categories.SMALL_STRAIGHT:
                    return InsertScore(CalcSmallStraight(dice), categories);
                case Categories.LARGE_STRAIGHT:
                    return InsertScore(CalcLargeStraight(dice), categories);
                case Categories.YAHTZEE:
                    return InsertScore(CalcYahtzee(dice), categories);
                case Categories.CHANCE:
                    return InsertScore(CalcChance(dice), categories);
                default:
                    throw new ArgumentException();
            }
        }

        /// <summary>
        /// Simulates a roll of <c>diceCount</c> dice.
        /// </summary>
        /// <param name="diceCount">The number of dice to roll.</param>
        /// <returns>An array of dice represented by integer values.</returns>
        public int[] Throw(int diceCount)
        {
            if (diceCount < 0 || diceCount > 5)
            {
                throw new ArgumentOutOfRangeException();
            }
            int[] dice = new int[diceCount];
            Random random = new Random();
            for (int i = 0; i < diceCount; i++)
            {
                dice[i] = random.Next(6) + 1;
            }
            return dice;
        }

        /// <summary>
        /// This function counts the corresponding numbers.
        /// </summary>
        /// <param name="array">The dice input array.</param>
        /// <param name="category">The number, which should be counted.</param>
        /// <returns>The reached score.</returns>
        public int CalcCounter(int[] dice, Categories category)
        {
            int number = (int)category + 1;
            int sum = 0;
            foreach (var die in dice)
            {
                if (die == number) sum += die;
            }
            return sum;
        }

        public int CalcThreeOfAKind(int[] dice)
        {
            int[] counts = histogram(dice);
            foreach (int count in counts)
            {
                if (count >= 3) return sum(dice);
            }
            return 0;
        }

        public int CalcFourOfAKind(int[] dice)
        {
            int[] counts = histogram(dice);
            foreach (int count in counts)
            {
                if (count >= 4) return sum(dice);
            }
            return 0;
        }

        /// <summary>
        /// Checks if the combinaiton of dice is a Full House.
        /// </summary>
        /// <param name="dice">The dice input array.</param>
        /// <returns>The reached score.</returns>
        public int CalcFullHouse(int[] dice)
        {
            int[] counts = histogram(dice);
            bool found2 = false;
            bool found3 = false;

            foreach (int count in counts)
            {
                if (count == 2) found2 = true;
                else if (count == 3) found3 = true;
            }
            return (found2 && found3) ? 25 : 0;
        }

        /// <summary>
        /// Calculates the score for a small straight.
        /// </summary>
        /// <param name="dice">All active dice values.</param>
        /// <returns>The score.</returns>
        public int CalcSmallStraight(int[] dice)
        {
            int sequential = 1;
            Array.Sort(dice);
            for (int i = 1; i < dice.Length; i++)
            {
                int diff = dice[i] - dice[i - 1];

                if (diff == 0) continue;
                else if (diff == 1) sequential++;
                else sequential = 1;
            }
            return (sequential >= 4) ? 30 : 0;
        }

        /// <summary>
        /// Calculates the score for a large straight.
        /// </summary>
        /// <param name="dice">All active dice values.</param>
        /// <returns>The score.</returns>
        public int CalcLargeStraight(int[] dice)
        {
            Array.Sort(dice);
            for (int i = 1; i < dice.Length; i++)
            {
                if (dice[i] - dice[i - 1] != 1) return 0;
            }
            return 40;
        }

        /// <summary>
        /// Checks if the values of the input array are all the same.
        /// </summary>
        /// <param name="dice">The dice input array.</param>
        /// <returns>The reached score.</returns>
        public int CalcYahtzee(int[] dice)
        {
            for (int i = 0; i < dice.Length - 1; i++)
            {
                if (dice[i] != dice[i + 1]) return 0;
            }
            return 50;
        }

        /// <summary>
        /// Calculates the sum of all dice.
        /// </summary>
        /// <param name="dice">The dice input array.</param>
        /// <returns>The reached score.</returns>
        public int CalcChance(int[] dice)
        {
            return sum(dice);
        }

        private int sum(int[] dice)
        {
            int sum = 0;
            foreach (int die in dice)
            {
                sum += die;
            }
            return sum;
        }

        private int[] histogram(int[] dice)
        {
            int[] counts = new int[6 + 1];
            foreach (int die in dice)
            {
                counts[die]++;
            }
            return counts;
        }
    }
}
