﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Kniffel.Lib;
using Kniffel.Server.Services;

namespace Kniffel.Server.Controllers
{
    [Route("kniffel")]
    [ApiController]
    public class KniffelController : ControllerBase
    {
        private readonly IDice _dice;

        public KniffelController(IDice dice)
        {
            _dice = dice;
        }

        [HttpGet("throw/{count}")]
        public ActionResult<int[]> GetDiceThrow(int count)
        {
            try
            {
                return Ok(_dice.Throw(count));
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest("Naughty");
            }
        }

        [HttpPost("calculate/{category}")]
        public ActionResult<Dictionary<Categories, int?>> PostCalc([FromBody] int[] diceCombination, int category)
        {
            var cat = (Categories)category;
            try
            {
                //return CreatedAtAction(cat.ToString(), _dice.Calculate(diceCombination, cat));
                return Ok(_dice.Calculate(diceCombination, cat));
            } catch(ArgumentException)
            {
                return BadRequest("Naughty");
            }
        }

        /// <summary>
        /// This Function resets the active game, for the player.
        /// </summary>
        /// <returns></returns>
        [HttpGet("reset")]
        public ActionResult<Dictionary<Categories, int?>> GetReset()
        {
            Program.dataStorage = new Dictionary<Categories, int?>();
            foreach (Categories categories in Enum.GetValues(typeof(Categories)))
            {
                Program.dataStorage.Add(categories, null);
            }
            return (Ok(Program.dataStorage));
        }
    }
}