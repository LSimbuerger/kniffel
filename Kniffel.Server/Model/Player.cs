﻿using Kniffel.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kniffel.Server.Services
{
    public class Player
    {
        public int ID { get;}
        public IDictionary<Categories, int?> Score { get; }
        
        private IStorage _GameStorage;

        public Player(int ID, IStorage storage)
        {
            if (ID <= 0) throw new ArgumentOutOfRangeException();
            if (storage == null) throw new ArgumentNullException();

            this.ID = ID;
            this._GameStorage = storage;

            this.Score = _GameStorage.GetActiveGameStorage(this.ID);
        }
    }
}
