﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kniffel.Lib
{
    public interface IStorage
    {
        /// <summary>
        /// This will reset the game storage for the player, with the requested id.
        /// </summary>
        /// <param name="id">The id of the player, which game storage should be deleted.</param>
        /// <returns>An empty game storage.</returns>
        IDictionary<Categories, int?> ResetActiveGameStorage(int id);
        /// <summary>
        /// This will return the game storage for the given palyer id.
        /// </summary>
        /// <param name="id">The id of the player, which game storage should be returned.</param>
        /// <returns>The active game storage for the given player.</returns>
        IDictionary<Categories, int?> GetActiveGameStorage(int id);
        /// <summary>
        /// This will enter a new score into the game storage.
        /// </summary>
        /// <param name="id">The player id.</param>
        /// <param name="categories">The kniffel categorie, where the score should be saved.</param>
        /// <param name="score">The calculated score.</param>
        void EnterScore(int id, Categories categories, int score);
    }
}
