﻿using System.Collections.Generic;

namespace Kniffel.Lib
{
    public interface IDice
    {
        /// <summary>
        /// Rolls a given number of dice.
        /// </summary>
        /// <param name="diceCount">Number of dice to throw.</param>
        /// <returns>An integer array of dice.</returns>
        int[] Throw(int diceCount);

        /// <summary>
        /// Calculates a score of the throw.
        /// </summary>
        /// <param name="dice">Dice thrown by the player.</param>
        /// <param name="category">Chosen category.</param>
        /// <returns>A dictionary representing the player's scorecard.</returns>
        IDictionary<Categories, int?> Calculate(int[] dice, Categories category);
    }
}
