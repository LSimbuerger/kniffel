﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kniffel.Lib
{
    /// <summary>
    /// Enum for all available categories in a kniffel game.
    /// </summary>
    public enum Categories
    {
        ACES,
        TWOS,
        THREES,
        FOURS,
        FIVES,
        SIXES,
        THREE_OF_A_KIND,
        FOUR_OF_A_KIND,
        FULL_HOUSE,
        SMALL_STRAIGHT,
        LARGE_STRAIGHT,
        YAHTZEE,
        CHANCE 
    }
}
