using Kniffel.Lib;
using Kniffel.Server.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Kniffel.Test.Controller
{
    public class UnitTest1
    {


        [Fact]
        public void GetDiceThrown_Invalid_Input()
        {
            // Arrange.
            var mockDice = new Mock<IDice>();
            int[] possibleValues = { 1, 2, 3, 4, 5 };
            mockDice.Setup(x => x.Throw(It.IsNotIn(possibleValues))).Throws(new ArgumentOutOfRangeException());
            var controller = new KniffelController(mockDice.Object);

            // Act.
            var have = controller.GetDiceThrow(7).Result;

            // Assert.
            Assert.IsType<BadRequestObjectResult>(have);
        }

        [Theory]
        [InlineData(new int[] { 1, 1, 1, 2, 2 })]
        [InlineData(new int[] { 4, 4, 4, 2 })]
        [InlineData(new int[] { 1, 1, 1 })]
        [InlineData(new int[] { 3, 3 })]
        [InlineData(new int[] { 1 })]
        public void GetDiceThrown_Return_Right_Size(int[] input)
        {
            // Arrange.
            var mockDice = new Mock<IDice>();
            var possibleValues = new int[] { 1, 2, 3, 4, 5 };
            mockDice.Setup(x => x.Throw(It.IsIn(possibleValues))).Returns(input);
            var controller = new KniffelController(mockDice.Object);

            // Act.
            var have = (OkObjectResult)controller.GetDiceThrow(input.Length).Result;

            // Assert.
            Assert.Equal(input, have.Value);

        }

        [Fact]
        public void PostCalculation_Invalid_Request()
        {
            // Arrange.
            var mockDice = new Mock<IDice>();
            int[] haveArray = { 1, 2, 3, 4, 5 };

            mockDice.Setup(x => x.Calculate(
                It.Is<int[]>(j => j.Length == 5),
                It.IsAny<Categories>()))
                .Throws(new ArgumentOutOfRangeException());

            var controller = new KniffelController(mockDice.Object);

            // Act.
            var have = controller.PostCalc(haveArray, (int)Categories.ACES).Result;

            // Assert.
            Assert.IsType<BadRequestObjectResult>(have);
        }


        // TODO: not finished !!!
        [Theory]
        [InlineData(Categories.YAHTZEE, 50)]
        [InlineData(Categories.THREE_OF_A_KIND, 8)]
        [InlineData(Categories.LARGE_STRAIGHT, 29)]
        [InlineData(Categories.THREE_OF_A_KIND, 0)]
        [InlineData(Categories.FOUR_OF_A_KIND, 29)]
        [InlineData(Categories.ACES, 0)]
        public void PostCalculation( Categories selectedCategories, int score)
        {
            // Arrange.
            int[] diceThrow = { 1, 2, 3, 4, 5 };

            var testDictionary = new Dictionary<Categories, int?>();
            foreach (Categories categories in Enum.GetValues(typeof(Categories)))
            {
                testDictionary.Add(categories, null);
            }

            var mockDice = new Mock<IDice>();
            mockDice.Setup(x => x.Calculate(
                It.Is<int[]>(j => j.Length == 5),
                It.IsAny<Categories>()))
                .Returns(() => {
                    testDictionary[selectedCategories] =  score;
                    return testDictionary;
                });

            var testController = new KniffelController(mockDice.Object);

            // Act.
            var result = (OkObjectResult)testController.PostCalc(diceThrow, score).Result;

           // var test = result.Value;
            
            // Assert.
            Assert.Equal(score, result.Value);
        }

    }
}

