﻿using Kniffel.Lib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kniffel.Client
{
    class Program
    {

        static void Main(string[] args)
        {
            Play();
        }
        
        public static void Play()
        {
            // TODO: Allow replacing dices.
            int throwDice = 5;
            int[] result = new int[5];
            IDice dice = new DiceProxy();

            for (int i = 0; i < 3; i++)
            {
                var randomDice = dice.Throw(throwDice);

                // Merge thrown dice with current dice.
                for(int j = 0; j < randomDice.Length; j++)
                {
                    result[j + (result.Length - throwDice)] = randomDice[j];
                }

                Console.WriteLine("Wurfergebnis: " + String.Join(", ", randomDice));
                Console.WriteLine("Aktuelle Würfel: " + String.Join(", ", result));
                Console.WriteLine("Welche Würfel sollen neu gewürfelt werden?");

                string take = Console.ReadLine();

                if (String.IsNullOrEmpty(take)) continue;

                var chosenDice = take.Split(' ').Select(b => int.Parse(b)).ToList();
                throwDice = chosenDice.Count();

                // Check if input is valid.
                bool invalid = false;
                foreach(var chosen in chosenDice)
                {
                    if(!result.Contains(chosen))
                    {
                        invalid = true;
                        break;
                    }
                }
                if (invalid) continue;

                // Sort remaining dice for next merge.
                var temp = new int[5];
                int index = 0;
                for(int j = 0; j < result.Length; j++)
                {
                    if(!chosenDice.Contains(result[j]))
                    {
                        temp[index++] = result[j];
                    } else
                    {
                        chosenDice.Remove(result[j]);
                    }
                }
                result = temp;
            }
            Console.WriteLine(String.Join(", ", result));
        }
    }
}
