﻿using Kniffel.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Kniffel.Client
{
    public class DiceProxy: IDice
    {
        private HttpClient client { get; set; }
        public DiceProxy()
        {
            client = new HttpClient();
        }

        public IDictionary<Categories, int?> Calculate(int[] dice, Categories category)
        {
            var content = new StringContent(JsonConvert.SerializeObject(dice));
            var cat = (int)category;
            var response = client.PostAsync($"http://kniffel.azurewebsites.net/kniffel/calculate/{cat}", content).Result;
            var json = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<Dictionary<Categories, int?>>(json);
        }

        public int[] Throw(int diceCount)
        {
            string json = client.GetStringAsync($"https://kniffel.azurewebsites.net/kniffel/throw/{diceCount}").Result;
            return JsonConvert.DeserializeObject<int[]>(json);
        }
    }
}
